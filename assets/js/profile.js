console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);

// retrieve user details from local storage
fetch("https://secret-harbor-36877.herokuapp.com/api/users/details", {
	headers: {
		'Authorization': `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		
		let profileContainer = document.querySelector("#profileContainer");
		let enrollmentsContainer = document.querySelector("#enrollmentsContainer");
		
		let courseId = params.get('courseId');
		// console.log(courseId);
		// console.log(data.enrollments);

		// let courseArr = [];
		
		let courseData = data.enrollments.map(course => {
			fetch(`https://secret-harbor-36877.herokuapp.com/api/courses/${course.courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				// console.log(JSON.stringify(data.name));

				return (
					`	
						<tr>
							<td>Course: ${data.name}</td>
							<td>Course: ${course.enrolledOn}</td>
							<td>Course: ${course.status}</td>
						</tr>
		 			`
					)
			})
		})
		enrollmentsContainer.innerHTML = courseData;

		profileContainer.innerHTML = 

			`	
		 		<main class="container my-5">	
		 			<div id="profileContainer" class="row">
		 				<div class="col-md-12">
		 					<section class="jumbotron text-center my-5">		
		 						<h5> Name: ${data.firstName} ${data.lastName} </h5>
		 						<h5> Email: ${data.email} </h5>
		 						<h5> Mobile No: ${data.mobileNo} </h5>
		 					</section>		
		 				</div>
		 			</div>
		 		</main>
		 	`		
})
