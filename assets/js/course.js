console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);
let adminUser = localStorage.getItem("isAdmin");

console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector('#enrollContainer')

let token = localStorage.getItem('token');
console.log(userToken);


fetch(`https://secret-harbor-36877.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
		`

	document.querySelector("#enrollButton").addEventListener("click", () => {

		fetch('https://secret-harbor-36877.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				alert("Thank you for enrolling! See you in class!");
				window.location.replace("courses.html")

			} else {
				alert("something went wrong")
			}

		})

	})

})